unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Image1: TImage;
    Label1: TLabel;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1; text: string=''; switch: boolean=true; index: integer=0;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
     Form1.Image1.Visible:= NOT switch;
       Form1.Button5.Enable:=NOT switch;
       Form1.Button4.Enable:=NOT switch;
       Form1.Button3.Enable:=NOT switch;
       Form1.Button2.Enable:=NOT switch;
     switch := NOT switch;
     Label1.Caption:= text;
     Form1.Color:=clGreen;


end;

procedure TForm1.Button2Click(Sender: TObject);
begin
     Form1.Image1.Top:= Form1.Image1.Top + 10;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
     Form1.Image1.Top:= Form1.Image1.Top - 10;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
     Form1.Image1.Left:= Form1.Image1.Left + 10;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
     Form1.Image1.Left:= Form1.Image1.Left - 10;
end;

procedure TForm1.Memo1Change(Sender: TObject);
begin
     text := Form1.Memo1.Lines[0];
end;

end.

