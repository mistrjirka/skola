unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Image1: TImage;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Label1Click(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Label1Click(Sender: TObject);
begin

end;

procedure TForm1.Button1Click(Sender: TObject);
begin
     Form1.Image1.Top:=Form1.Image1.Top+10;
     Form1.Image1.Left:=Form1.Image1.Left+10;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
     Form1.Image1.Top:=Form1.Image1.Top-10;
     Form1.Image1.Left:=Form1.Image1.Left-10;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
     Form1.Image1.Top:=Form1.Image1.Top-10;
     Form1.Image1.Left:=Form1.Image1.Left+10;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
     Form1.Image1.Top:=Form1.Image1.Top+10;
     Form1.Image1.Left:=Form1.Image1.Left-10;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
     Form1.Label1.Visible = not Form1.Label1.Visible;
end;

procedure TForm1.Image1Click(Sender: TObject);
begin

end;



end.

