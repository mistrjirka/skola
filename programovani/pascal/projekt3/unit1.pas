unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Image1: TImage;
    procedure Button1Click(Sender: TObject);
  private

  public

  end;

type
  arrayOfImages = array [0..2] of string;

  type
   Animation = class
   private
      images: arrayOfImages;
      image: string;

   public
         constructor create(image1, image2, image3:string);
         constructor start();
end;

var
  Form1: TForm1; anim: Animation;

implementation

constructor Animation.create(image1, image2, image3:string);
begin
    images[0]:=image1;
    images[1]:=image2;
    images[2]:=image3;
end;

constructor Animation.start();
begin
     write('ahoj');
     for image in images do
         begin
         Form1.Image1.Picture.LoadFromFile(image);
         Sleep(1000);
         writeln(image);
         end;
end;

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
write('ahoj');
anim.create('first','druhy','treti');
anim.start();
end;

end.

